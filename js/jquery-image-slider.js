(function () {
"use strict";
//Engine for images
    $(document).ready(function() {
        $('.next').on('click', function () {
            var currentImg = $('.active');
            var nextImg = currentImg.next();

            if (nextImg.length) {
                currentImg.removeClass('active').css('z-index');
                nextImg.addClass('active').css('z-index');
            }
        });

        $('.prev').on('click', function () {
            var currentImg = $('.active');
            var prevImg = currentImg.prev();

            if (prevImg.length) {
                currentImg.removeClass('active').css('z-index');
                prevImg.addClass('active').css('z-index');
            }
        });
        // //Engine for textbox
        // $('.next').on('click', function () {
        //     var currentText = $('.textBox');
        //     var nextText = currentText.next();
        //
        //     if (currentText.length) {
        //         currentText.removeClass('textBox');
        //         nextText.addClass('textBox');
        //     }
        // });
        // $('.prev').on('click', function () {
        //     var currentText = $('.textBox');
        //     var prevText = currentText.prev();
        //
        //     if (prevText.length) {
        //         currentText.removeClass('textBox');
        //         prevText.addClass('textBox');
        //     }
        // });
    });
})()